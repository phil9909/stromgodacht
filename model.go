package stromgodacht

import "time"

// Response

type RegionStateRangeViewModel struct {
	States []RegionStateViewModel `json:"states"`
}

type RegionStateViewModel struct {
	From  time.Time   `json:"from"`
	To    time.Time   `json:"to"`
	State RegionState `json:"state"`
}

type RegionStateNowViewModel struct {
	State RegionState `json:"state"`
}

type RegionState int8

type ForecastViewModel struct {
	Load                []ForecastPointInTimeViewModel `json:"load"`
	RenewableEnergy     []ForecastPointInTimeViewModel `json:"renewableEnergy"`
	ResidualLoad        []ForecastPointInTimeViewModel `json:"residualLoad"`
	SuperGreenThreshold []ForecastPointInTimeViewModel `json:"superGreenThreshold"`
}

type ForecastPointInTimeViewModel struct {
	DateTime time.Time `json:"dateTime"`
	Value    float64   `json:"value"`
}

const (
	RegionStatesSuperGreen = RegionState(-1)
	RegionStateGreen       = RegionState(1)
	// RegionStateYellow deprecated has been removed
	RegionStateYellow = RegionState(2)
	RegionStateOrange = RegionState(3)
	RegionStateRed    = RegionState(4)
)

// Request

type StatesRelativeParams struct {
	Zip           string
	HoursInFuture int32
	HoursInPast   Optional[int32]
}

type StatesParams struct {
	Zip  string
	From time.Time
	To   time.Time
}

type NowParams struct {
	Zip string
}

type ForecastParams struct {
	Zip  string
	From Optional[time.Time]
	To   Optional[time.Time]
}

type Optional[T any] struct {
	t     T
	valid bool
}

func Some[T any](t T) Optional[T] {
	return Optional[T]{
		t:     t,
		valid: true,
	}
}
