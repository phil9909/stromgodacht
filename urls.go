package stromgodacht

import "net/url"

var (
	defaultBaseUrl = url.URL{
		Scheme: "https",
		Host:   "api.stromgedacht.de",
	}
)

func apiStateRelative(baseUrl url.URL) url.URL {
	apiStateRelative := baseUrl
	apiStateRelative.Path = "/v1/statesRelative"
	return apiStateRelative
}

func apiState(baseUrl url.URL) url.URL {
	apiStateRelative := baseUrl
	apiStateRelative.Path = "/v1/states"
	return apiStateRelative
}

func apiNow(baseUrl url.URL) url.URL {
	apiStateRelative := baseUrl
	apiStateRelative.Path = "/v1/now"
	return apiStateRelative
}

func apiForecast(baseUrl url.URL) url.URL {
	apiStateRelative := baseUrl
	apiStateRelative.Path = "/v1/forecast"
	return apiStateRelative
}
