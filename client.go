package stromgodacht

import (
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"net/url"
	"time"
)

type Client interface {
	Forecast(params ForecastParams) (ForecastViewModel, error)
	Now(params NowParams) (RegionStateNowViewModel, error)
	States(params StatesParams) (RegionStateRangeViewModel, error)
	StatesRelative(params StatesRelativeParams) (RegionStateRangeViewModel, error)
}

type ClientOption interface {
	apply(c *client) error
}

type client struct {
	b2bId   *string
	baseUrl url.URL
}

func (c *client) Forecast(params ForecastParams) (ForecastViewModel, error) {
	url := apiForecast(c.baseUrl)
	query := url.Query()
	query.Add("zip", params.Zip)
	if params.From.valid {
		query.Add("from", params.From.t.Format(time.DateOnly))
	}
	if params.From.valid {
		query.Add("to", params.To.t.Format(time.DateOnly))
	}
	url.RawQuery = query.Encode()

	return fetch[ForecastViewModel](c, url.String())
}

func (c *client) Now(params NowParams) (RegionStateNowViewModel, error) {
	url := apiNow(c.baseUrl)
	query := url.Query()
	query.Add("zip", params.Zip)
	url.RawQuery = query.Encode()

	return fetch[RegionStateNowViewModel](c, url.String())
}

func (c *client) States(params StatesParams) (RegionStateRangeViewModel, error) {
	url := apiState(c.baseUrl)
	query := url.Query()
	query.Add("zip", params.Zip)
	query.Add("from", params.From.Format(time.RFC3339))
	query.Add("to", params.To.Format(time.RFC3339))
	url.RawQuery = query.Encode()

	return fetch[RegionStateRangeViewModel](c, url.String())
}

func (c *client) StatesRelative(params StatesRelativeParams) (RegionStateRangeViewModel, error) {
	url := apiStateRelative(c.baseUrl)
	query := url.Query()
	query.Add("zip", params.Zip)
	query.Add("hoursInFuture", fmt.Sprintf("%d", params.HoursInFuture))
	if params.HoursInPast.valid {
		query.Add("hoursInPast", fmt.Sprintf("%d", params.HoursInPast.t))
	}
	url.RawQuery = query.Encode()

	return fetch[RegionStateRangeViewModel](c, url.String())
}

func fetch[T any](c *client, url string) (T, error) {
	var result T

	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		return result, err
	}
	req.Header.Set("Accept", "application/json")
	if c.b2bId != nil {
		req.Header.Set("X-B2B-ID", *c.b2bId)
	}

	response, err := http.DefaultClient.Do(req)
	if err != nil {
		return result, err
	}
	defer response.Body.Close()

	if response.StatusCode != http.StatusOK {
		body, err := io.ReadAll(response.Body)
		if err != nil {
			return result, err
		}
		return result, fmt.Errorf("unexpected status %q: %s", response.Status, string(body))
	}

	err = json.NewDecoder(response.Body).Decode(&result)
	if err != nil {
		return result, err
	}

	return result, nil
}

type clientOption func(c *client) error

func (f clientOption) apply(c *client) error {
	return f(c)
}

func WithB2BID(b2bID string) ClientOption {
	return clientOption(func(c *client) error {
		c.b2bId = &b2bID
		return nil
	})
}

func WithBaseUrl(baseUrl string) ClientOption {
	return clientOption(func(c *client) error {
		parsedUrl, err := url.Parse(baseUrl)
		if err != nil {
			return err
		}
		c.baseUrl = *parsedUrl
		return nil
	})
}

func NewClient(options ...ClientOption) (Client, error) {
	client := client{
		baseUrl: defaultBaseUrl,
	}
	for _, option := range options {
		err := option.apply(&client)
		if err != nil {
			return nil, err
		}
	}
	return &client, nil
}
