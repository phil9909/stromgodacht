package stromgodacht_test

import (
	"testing"
	"time"

	. "github.com/onsi/gomega"
	"github.com/onsi/gomega/ghttp"
	"gitlab.com/phil9909/stromgodacht"
)

func TestStatesRelative(t *testing.T) {
	g := NewWithT(t)
	http := ghttp.NewGHTTPWithGomega(g)
	server := ghttp.NewServer()

	defer server.Close()

	server.AppendHandlers(
		ghttp.CombineHandlers(
			http.VerifyRequest("GET", "/v1/statesRelative", "zip=88250&hoursInFuture=24"),
			http.VerifyHeaderKV("accept", "application/json"),
			http.RespondWith(200, `{
				"states": [
					{
						"from": "2024-05-14T19:52:41.9174182+02:00",
						"to": "2024-05-15T00:00:00+02:00",
						"state": 1
					},
					{
						"from": "2024-05-15T00:00:00+02:00",
						"to": "2024-05-15T05:00:00+02:00",
						"state": -1
					},
					{
						"from": "2024-05-15T05:00:00+02:00",
						"to": "2024-05-16T20:52:41.9174182+02:00",
						"state": 1
					}
				]
			}`),
		),
	)

	client, err := stromgodacht.NewClient(stromgodacht.WithBaseUrl(server.URL()))
	g.Expect(err).NotTo(HaveOccurred())

	res, err := client.StatesRelative(stromgodacht.StatesRelativeParams{
		Zip:           "88250",
		HoursInFuture: 24,
	})
	g.Expect(err).NotTo(HaveOccurred())
	g.Expect(res.States).To(HaveLen(3))
	g.Expect(res.States[0].State).To(Equal(stromgodacht.RegionStateGreen))
	g.Expect(res.States[1].State).To(Equal(stromgodacht.RegionStatesSuperGreen))
	g.Expect(res.States[2].State).To(Equal(stromgodacht.RegionStateGreen))
}

func TestStatesRelativeWithHoursInPast(t *testing.T) {
	g := NewWithT(t)
	http := ghttp.NewGHTTPWithGomega(g)
	server := ghttp.NewServer()

	defer server.Close()

	server.AppendHandlers(
		ghttp.CombineHandlers(
			http.VerifyRequest("GET", "/v1/statesRelative", "zip=88250&hoursInFuture=24&hoursInPast=1"),
			http.VerifyHeaderKV("accept", "application/json"),
			http.RespondWith(200, `{
				"states": [
					{
						"from": "2024-05-14T19:52:41.9174182+02:00",
						"to": "2024-05-15T00:00:00+02:00",
						"state": 1
					},
					{
						"from": "2024-05-15T00:00:00+02:00",
						"to": "2024-05-15T05:00:00+02:00",
						"state": -1
					},
					{
						"from": "2024-05-15T05:00:00+02:00",
						"to": "2024-05-16T20:52:41.9174182+02:00",
						"state": 1
					}
				]
			}`),
		),
	)

	client, err := stromgodacht.NewClient(stromgodacht.WithBaseUrl(server.URL()))
	g.Expect(err).NotTo(HaveOccurred())

	res, err := client.StatesRelative(stromgodacht.StatesRelativeParams{
		Zip:           "88250",
		HoursInFuture: 24,
		HoursInPast:   stromgodacht.Some[int32](1),
	})
	g.Expect(err).NotTo(HaveOccurred())
	g.Expect(res.States).To(HaveLen(3))
	g.Expect(res.States[0].State).To(Equal(stromgodacht.RegionStateGreen))
	g.Expect(res.States[1].State).To(Equal(stromgodacht.RegionStatesSuperGreen))
	g.Expect(res.States[2].State).To(Equal(stromgodacht.RegionStateGreen))
}

func TestStatesRelativeError(t *testing.T) {
	g := NewWithT(t)
	http := ghttp.NewGHTTPWithGomega(g)
	server := ghttp.NewServer()

	defer server.Close()

	server.AppendHandlers(
		ghttp.CombineHandlers(
			http.VerifyRequest("GET", "/v1/statesRelative", "zip=99999&hoursInFuture=24"),
			http.VerifyHeaderKV("accept", "application/json"),
			http.RespondWith(400, `"No data is available for the specified zip code (99999)."`),
		),
	)

	client, err := stromgodacht.NewClient(stromgodacht.WithBaseUrl(server.URL()))
	g.Expect(err).NotTo(HaveOccurred())

	res, err := client.StatesRelative(stromgodacht.StatesRelativeParams{
		Zip:           "99999",
		HoursInFuture: 24,
	})
	g.Expect(err).To(MatchError(Equal("unexpected status \"400 Bad Request\": \"No data is available for the specified zip code (99999).\"")))
	g.Expect(res.States).To(BeEmpty())
}

func TestStates(t *testing.T) {
	g := NewWithT(t)
	http := ghttp.NewGHTTPWithGomega(g)
	server := ghttp.NewServer()

	defer server.Close()

	server.AppendHandlers(
		ghttp.CombineHandlers(
			http.VerifyRequest("GET", "/v1/states", "zip=88250&from=2024-05-14T00%3A00%3A00%2B02%3A00&to=2024-05-16T23%3A59%3A59%2B02%3A00"),
			http.VerifyHeaderKV("accept", "application/json"),
			http.RespondWith(200, `{
				"states": [
				  {
					"from": "2024-05-14T00:00:00+02:00",
					"to": "2024-05-14T10:00:00+02:00",
					"state": 1
				  },
				  {
					"from": "2024-05-14T10:00:00+02:00",
					"to": "2024-05-14T18:00:00+02:00",
					"state": -1
				  },
				  {
					"from": "2024-05-14T18:00:00+02:00",
					"to": "2024-05-15T00:00:00+02:00",
					"state": 1
				  },
				  {
					"from": "2024-05-15T00:00:00+02:00",
					"to": "2024-05-15T05:00:00+02:00",
					"state": -1
				  },
				  {
					"from": "2024-05-15T05:00:00+02:00",
					"to": "2024-05-16T23:59:59+02:00",
					"state": 1
				  }
				]
			}`),
		),
	)

	client, err := stromgodacht.NewClient(stromgodacht.WithBaseUrl(server.URL()))
	g.Expect(err).NotTo(HaveOccurred())

	location, err := time.LoadLocation("Europe/Berlin")
	g.Expect(err).NotTo(HaveOccurred())

	res, err := client.States(stromgodacht.StatesParams{
		Zip:  "88250",
		From: time.Date(2024, time.May, 14, 0, 0, 0, 0, location),
		To:   time.Date(2024, time.May, 16, 23, 59, 59, 0, location),
	})
	g.Expect(err).NotTo(HaveOccurred())
	g.Expect(res.States).To(HaveLen(5))
	g.Expect(res.States[0].State).To(Equal(stromgodacht.RegionStateGreen))
	g.Expect(res.States[1].State).To(Equal(stromgodacht.RegionStatesSuperGreen))
	g.Expect(res.States[2].State).To(Equal(stromgodacht.RegionStateGreen))
	g.Expect(res.States[3].State).To(Equal(stromgodacht.RegionStatesSuperGreen))
	g.Expect(res.States[4].State).To(Equal(stromgodacht.RegionStateGreen))
}

func TestStatesError(t *testing.T) {
	g := NewWithT(t)
	http := ghttp.NewGHTTPWithGomega(g)
	server := ghttp.NewServer()

	defer server.Close()

	server.AppendHandlers(
		ghttp.CombineHandlers(
			http.VerifyRequest("GET", "/v1/states", "zip=99999&from=2024-05-14T00%3A00%3A00%2B02%3A00&to=2024-05-16T23%3A59%3A59%2B02%3A00"),
			http.VerifyHeaderKV("accept", "application/json"),
			http.RespondWith(400, `"No data is available for the specified zip code (99999)."`),
		),
	)

	client, err := stromgodacht.NewClient(stromgodacht.WithBaseUrl(server.URL()))
	g.Expect(err).NotTo(HaveOccurred())

	location, err := time.LoadLocation("Europe/Berlin")
	g.Expect(err).NotTo(HaveOccurred())

	res, err := client.States(stromgodacht.StatesParams{
		Zip:  "99999",
		From: time.Date(2024, time.May, 14, 0, 0, 0, 0, location),
		To:   time.Date(2024, time.May, 16, 23, 59, 59, 0, location),
	})

	g.Expect(err).To(MatchError(Equal("unexpected status \"400 Bad Request\": \"No data is available for the specified zip code (99999).\"")))
	g.Expect(res.States).To(BeEmpty())
}

func TestNow(t *testing.T) {
	g := NewWithT(t)
	http := ghttp.NewGHTTPWithGomega(g)
	server := ghttp.NewServer()

	defer server.Close()

	server.AppendHandlers(
		ghttp.CombineHandlers(
			http.VerifyRequest("GET", "/v1/now", "zip=88250"),
			http.VerifyHeaderKV("accept", "application/json"),
			http.RespondWith(200, `{
				"state": 1
			}`),
		),
	)

	client, err := stromgodacht.NewClient(stromgodacht.WithBaseUrl(server.URL()))
	g.Expect(err).NotTo(HaveOccurred())

	res, err := client.Now(stromgodacht.NowParams{
		Zip: "88250",
	})
	g.Expect(err).NotTo(HaveOccurred())
	g.Expect(res.State).To(Equal(stromgodacht.RegionStateGreen))
}

func TestNowError(t *testing.T) {
	g := NewWithT(t)
	http := ghttp.NewGHTTPWithGomega(g)
	server := ghttp.NewServer()

	defer server.Close()

	server.AppendHandlers(
		ghttp.CombineHandlers(
			http.VerifyRequest("GET", "/v1/now", "zip=99999"),
			http.VerifyHeaderKV("accept", "application/json"),
			http.RespondWith(400, `"No data is available for the specified zip code (99999)."`),
		),
	)

	client, err := stromgodacht.NewClient(stromgodacht.WithBaseUrl(server.URL()))
	g.Expect(err).NotTo(HaveOccurred())

	res, err := client.Now(stromgodacht.NowParams{
		Zip: "99999",
	})

	g.Expect(err).To(MatchError(Equal("unexpected status \"400 Bad Request\": \"No data is available for the specified zip code (99999).\"")))
	g.Expect(res).To(Equal(stromgodacht.RegionStateNowViewModel{}))
}

func TestForecast(t *testing.T) {
	g := NewWithT(t)
	http := ghttp.NewGHTTPWithGomega(g)
	server := ghttp.NewServer()

	defer server.Close()

	server.AppendHandlers(
		ghttp.CombineHandlers(
			http.VerifyRequest("GET", "/v1/forecast", "zip=88250&from=2024-05-12&to=2024-05-13"),
			http.VerifyHeaderKV("accept", "application/json"),
			http.RespondWith(200, `{
				"load": [
					{
						"dateTime": "2024-05-12T00:00:00Z",
						"value": 4633
					},
					{
						"dateTime": "2024-05-12T00:15:00Z",
						"value": 4529
					}
				],
				"renewableEnergy": [
					{
						"dateTime": "2024-05-12T00:00:00Z",
						"value": 1344.217
					},
					{
						"dateTime": "2024-05-12T00:15:00Z",
						"value": 1334.217
					}
				],
				"residualLoad": [
					{
						"dateTime": "2024-05-12T00:00:00Z",
						"value": 3288.783
					},
					{
						"dateTime": "2024-05-12T00:15:00Z",
						"value": 3194.783
					}
				],
				"superGreenThreshold": [
					{
						"dateTime": "2024-05-12T00:00:00Z",
						"value": 2337
					},
					{
						"dateTime": "2024-05-12T00:15:00Z",
						"value": 2322
					}
				]
			}`),
		),
	)

	client, err := stromgodacht.NewClient(stromgodacht.WithBaseUrl(server.URL()))
	g.Expect(err).NotTo(HaveOccurred())

	res, err := client.Forecast(stromgodacht.ForecastParams{
		Zip:  "88250",
		From: stromgodacht.Some(time.Date(2024, time.May, 12, 0, 0, 0, 0, time.Local)),
		To:   stromgodacht.Some(time.Date(2024, time.May, 13, 0, 0, 0, 0, time.Local)),
	})
	g.Expect(err).NotTo(HaveOccurred())
	g.Expect(res.Load).To(HaveLen(2))
	g.Expect(res.RenewableEnergy).To(HaveLen(2))
	g.Expect(res.ResidualLoad).To(HaveLen(2))
	g.Expect(res.SuperGreenThreshold).To(HaveLen(2))
}

func TestForecastError(t *testing.T) {
	g := NewWithT(t)
	http := ghttp.NewGHTTPWithGomega(g)
	server := ghttp.NewServer()

	defer server.Close()

	server.AppendHandlers(
		ghttp.CombineHandlers(
			http.VerifyRequest("GET", "/v1/forecast", "zip=99999"),
			http.VerifyHeaderKV("accept", "application/json"),
			http.RespondWith(400, `"No data is available for the specified zip code (99999)."`),
		),
	)

	client, err := stromgodacht.NewClient(stromgodacht.WithBaseUrl(server.URL()))
	g.Expect(err).NotTo(HaveOccurred())

	res, err := client.Forecast(stromgodacht.ForecastParams{
		Zip: "99999",
	})
	g.Expect(err).To(MatchError(Equal("unexpected status \"400 Bad Request\": \"No data is available for the specified zip code (99999).\"")))
	g.Expect(res).To(Equal(stromgodacht.ForecastViewModel{}))
}

func TestInvalidUrl(t *testing.T) {
	g := NewWithT(t)

	client, err := stromgodacht.NewClient(stromgodacht.WithBaseUrl("invalid\nurl"))

	g.Expect(err).To(MatchError(ContainSubstring("invalid control character in URL")))
	g.Expect(client).To(BeNil())
}

func TestFtpUrl(t *testing.T) {
	g := NewWithT(t)

	client, err := stromgodacht.NewClient(stromgodacht.WithBaseUrl("ftp://example.com"))
	g.Expect(err).NotTo(HaveOccurred())

	res, err := client.Now(stromgodacht.NowParams{
		Zip: "66666",
	})

	g.Expect(err).To(MatchError(ContainSubstring("unsupported protocol scheme")))
	g.Expect(res).To(Equal(stromgodacht.RegionStateNowViewModel{}))
}
